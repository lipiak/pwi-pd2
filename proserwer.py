# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config
import os

from mimetypes import MimeTypes



def handle(request, logger):

    mime = MimeTypes()
    #sprawdzic czy zapytanie jest zapytaniem GET HTTP
    podzielone = request.split(' ')

    if podzielone[0] == 'GET' and (podzielone[2])[:4] == 'HTTP':

        #sprawdzic, czy plik lub katalog, ktorego zadamy istnieje
        zadany_plik = 'web' + podzielone[1]

        if os.path.exists(zadany_plik):

            #teraz dwie opcje, jesli katalog, wylistowac pliki
            #jest plik, otworzyc i zwrocic zawartosc z content typem

            #jesli to jest tylko katalog
            try:
                if os.path.isdir(zadany_plik[:-1]) or os.path.isdir(zadany_plik):
                    wszystko = os.listdir(zadany_plik)

                    content = '<ul>'
                    #generowanie odpowiedzi
                    for plik in wszystko:

                        #czy byl / na koncu zapytania
                        if zadany_plik[-1] == '/':
                            content += '<li><a href="' + podzielone[1] + plik + '">' + plik + '</a></li>'
                        else:
                            content += '<li><a href="' + podzielone[1] + '/' + plik + '">' + plik + '</a></li>'


                    content += '</ul>'

                    #kompletowanie odpowiedzi od serwera
                    header = 'HTTP/1.1 200 OK \r\n'
                    metadata = 'Content-Type: text/html \r\n'
                    return header + metadata + '\r\n' + content + '\r\n'

            finally:
                #return 'HTTP/1.1 505 Internal Server Error\r\n'

                #a moze to jest plik a nie katalog?
                if os.path.isfile(zadany_plik):
                    header = 'HTTP/1.1 200 OK\r\n'
                    metadata = 'Content-Type: ' + mime.guess_type(zadany_plik)[0] + '\r\n'

                    if zadany_plik.endswith('.png'):
                        metadata = 'Content-Type: image/png\r\n'

                    content = open(zadany_plik, 'rb').read()
                    return header + metadata + '\r\n' + content + '\r\n'

        else:
            return 'HTTP/1.1 404 NOT FOUND\r\n\r\n404 NOT FOUND'

    else:
        return 'HTTP/1.1 403 BAD REQUEST\r\n\r\n403 BAD REQUEST'


def http_serve(server_socket, logger):

    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        logger.info(u'połączono z {0}:{1}'.format(*client_address))

        #odbieranie żądania
        request = ''
        done = False
        buffer = 4096

        while not done:
            part = connection.recv(buffer)

            if len(part) < buffer:
                done = True
            request += part

        try:
            connection.sendall(handle(request, logger))

        finally:
            # Zamknięcie połączenia
            connection.close()


def server(logger):
    """Server HTTP

    logger: mechanizm do logowania wiadomości
    """
    # Tworzenie gniazda TCP/IP
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Ustawienie ponownego użycia tego samego gniazda
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem
    server_address = ('194.29.175.240', 23678)
    #server_address = ('127.0.0.1', 23678)
    server_socket.bind(server_address)
    logger.info(u'uruchamiam server na {0}:{1}'.format(*server_address))

    # Nasłuchiwanie przychodzących połączeń
    server_socket.listen(1)

    #html = open('web/web_page.html').read()

    try:
        http_serve(server_socket, logger)

    finally:
        server_socket.close()


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('http_server')
    server(logger)
    sys.exit(0)
